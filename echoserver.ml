(*MODULE UNIX SERVIDOR TCP
1) criar um socket
2) bind associar socket a porta
3) ouvir na porta
4) aceitar conexao*)



(* #######ENVIRONMENT####### *)
(* Server welcome message *)
let wmessage   = " ###WELCOME TO OCAML ECHO SERVER VERSION 0.2###\n"

(* Question message *)
let qmessage   = "Type your name:"

(* Server's ip *)
let ipserver   = "10.7.5.137"

(* Server port *)
let port       = 1234

(* Max size of a line *)
let max_line   = 50

(* Max size of socket cue *)
let listenq    = 50

(* Max number of clients *)
let maxclients = 10

(* Size of the hash table with pids process
let pidclients = Hashtbl.create maxclients  *)


(* ###### SERVER ####### *)

let rec sig_child signal =
  let pid, status = Unix.waitpid [Unix.WNOHANG] (-1) in
  if pid == 0 then
    sig_child signal
  else
    (*DEBUG*)
    Printf.printf "Process %d exited\n%!" pid
    (**)



let server f =

  (* Server socket *)
  let sock = Unix.socket Unix.PF_INET Unix.SOCK_STREAM 0 in

  (* setsocketopt ??*)
  Unix.setsockopt sock Unix.SO_REUSEADDR true;

  (* Servers' ip *)
  let ip = Unix.inet_addr_of_string ipserver in

  let sockaddr = Unix.ADDR_INET (ip, port) in
  try
    Unix.bind sock sockaddr;

    (* listenq *)
    Unix.listen sock listenq;

    (* Set event *)
    Sys.set_signal (Sys.sigchld) (Sys.Signal_handle sig_child);

    f sock

  with Unix.Unix_error (e,_,_) ->
    Printf.printf "echoserver: %s\n" (Unix.error_message e)

(* the function bellow handle the talk between the server and the client *)
let rec talk sock =

  let clistring = String.create max_line in

  (* Sending a greeting message *)
  ignore (Unix.send sock qmessage  0 (String.length qmessage) []);

  (* Reading Client message *)
  let len = Unix.recv sock clistring 0 max_line [] in


  (* If client message > 0 the replay HI client message *)
  if len > 0 then begin
    (* Concatenating the client message with replay message *)
    let servereplay = (String.concat " " ["HI";clistring;"\n"]) in

    (* Sending the replay message *)
    ignore (Unix.send sock servereplay 0 (String.length servereplay) []);

    (* Recursion *)
    talk sock
  end

(* The function below handle the incoming connections *)
let rec conacc sock =

  (* If the server has received more connections than maxclients then
    the children processes must be finished or the server will not be able
    to recieve new connections. *)
  (* if Hashtbl.length pidclients > (maxclients - 1) then begin

    (* Waiting for children processes finish *)
    Hashtbl.iter (fun a b -> ignore (Unix.waitpid [] a)) pidclients;

    (* Clearing the hashtable *)
    Hashtbl.clear pidclients
  end
  else *)

  (* Accpeting the connection *)
  try
    let clisock, cliaddr = Unix.accept sock in

    (* Forking process *)
    let pid = Unix.fork () in

    if pid == 0 then begin
      (* Child process *)
      Unix.close sock;
      ignore (Unix.send clisock wmessage  0 (String.length wmessage) []);
      talk clisock;
    end
    else
      (* Father process *)
      begin
        Unix.close clisock;
        (* Adding the process pid to the hash table
        Hashtbl.add pidclients pid (); *)

        (* Recursion *)
        conacc sock
      end
  with  Unix.Unix_error (Unix.EINTR, _, _) ->
    conacc sock

let _ =
  server conacc
(* God is real, unless declared integer. *)
(* ocamlopt unix.cmxa -o echoserver echoserver.ml *)

