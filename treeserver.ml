(*MODULE UNIX SERVIDOR TCP
1) criar um socket
2) bind associar socket a porta
3) ouvir na porta
4) aceitar conexao*)


(*###Types###*)
type 'a tree = Folha
             | Ramif of ('a tree * 'a * 'a tree)

type 'a order = Create of 'a
              | Add of 'a
              | Print  of 'a tree
              | Clear

(* #######ENVIRONMENT####### *)
(* Server welcome message *)
let wmessage   = " ###WELCOME TO OCAML BINARY TREE SERVER VERSION 0.1###\n"

(* Question message *)
let qmessage   = "Type your name:"

(* Server's ip *)
let ipserver   = "10.7.5.136"

(* Server port *)
let port       = 1234

(* Max size of a line *)
let max_line   = 2

(* Max size of socket cue *)
let listenq    = 50

(* Max number of clients *)
let maxclients = 10

(* #### BINARY TREE #### *)
let cria_arvore x = Ramif (Folha, x, Folha)

let rec insere_valor t x =

  match t with
  | Folha ->  cria_arvore x
  | Ramif (esq, v, dir) ->
          if x < v then
            Ramif (insere_valor esq x, v, dir)
          else
            Ramif (esq, v, insere_valor dir x)

let rec imprime_em_ordem t print =

  match t with
  | Folha -> ()
  | Ramif (esq, v, dir) ->
            imprime_em_ordem esq print;
            print v;
            imprime_em_ordem dir print

let map_tree f t =
  let rec map t acc =

  match t with
  | Folha -> acc
  | Ramif (esq, v, dir) ->
         let res = f v in
         let acc = insere_valor acc res in
         let acc = map esq acc in
         map dir acc in
    map  Folha

let rec fold_tree f t z =

  match t with
  | Folha -> z
  | Ramif (esq, v, dir) ->
           let y = f z v in
           let w = fold_tree f y esq in
           fold_tree f w dir



(* ###### SERVER ####### *)

let rec sig_child signal =
  let pid, status = Unix.waitpid [Unix.WNOHANG] (-1) in
  if pid == 0 then
    sig_child signal
  else
    (*DEBUG*)
    Printf.printf "Process %d exited\n%!" pid
    (**)



let server f =

  (* Server socket *)
  let sock = Unix.socket Unix.PF_INET Unix.SOCK_STREAM 0 in

  (* setsocketopt ??*)
  Unix.setsockopt sock Unix.SO_REUSEADDR true;

  (* Servers' ip *)
  let ip = Unix.inet_addr_of_string ipserver in

  let sockaddr = Unix.ADDR_INET (ip, port) in
  try
    Unix.bind sock sockaddr;

    (* listenq *)
    Unix.listen sock listenq;

    (* Set event *)
    Sys.set_signal (Sys.sigchld) (Sys.Signal_handle sig_child);

    f sock

  with Unix.Unix_error (e,_,_) ->
    Printf.printf "echoserver: %s\n" (Unix.error_message e)


let send_message sock message =
  ignore (Unix.send sock message  0 (String.length message) [])

exception MessageError

let recv_message sock max =
  let rmessage = String.create max  in

  let len = Unix.recv sock rmessage 0 max [] in

  if len > 0 then
    rmessage, len
  else
    raise MessageError


(*let show_tree t sock send  =


  fold_tree t sock send_message                      *)

(* the function bellow handle the talk between the server and the client *)
let rec talk sock t =

  (* Sending a greeting message *)
  send_message sock qmessage;

  (* Reading Client message *)
  let rmessage, _ = recv_message sock max_line in


    (* actions value *)
    let a, v = rmessage.[0], rmessage.[1] in
    match a with
    | 'c' -> let t = cria_arvore (int_of_char v) in

    let servereplay = (String.concat " " ["the tree";v;" created\n"]) in

    send_message sock servereplay;

    (* fold_tree (send_message sock) t (); *)
    (*show_tree t sock send_message;*)

    (* Concatenating the client message with replay message
    let servereplay = (String.concat " " ["HI";rmessage;"\n"]) in

    (* Sending the replay message *)
    send_message sock servereplay; *)

    (* Recursion *)
    talk sock t

(* The function below handle the incoming connections *)
let rec conacc sock =

  (* Accpeting the connection *)
  try
    let clisock, cliaddr = Unix.accept sock in

    (* Forking process *)
    let pid = Unix.fork () in

    if pid == 0 then begin
      (* Child process *)
      Unix.close sock;
      let t = ref Folha in
      send_message clisock wmessage;
      talk clisock t;
    end
    else
      (* Father process *)
      begin
        Unix.close clisock;

        (* Recursion *)
        conacc sock
      end
  with  Unix.Unix_error (Unix.EINTR, _, _) ->
    conacc sock


 let _ =
   server conacc

(* God is real, unless declared integer. *)
(* ocamlc -g unix.cma -o treeserver treeserver.ml *)
